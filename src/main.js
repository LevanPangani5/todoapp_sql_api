const express = require("express");
const makeStoppable = require("stoppable")
const http = require("http");
const cookeiSession= require('cookie-session');
const bodyParser = require('body-parser');
const routes = require('./routes')
const authRoutes = require('./routes/auth')

const app = express();
require('./lib/db');
app.use(
  cookeiSession({
    name:'session',
    keys:['sdfax123']
  })
)

app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

app.use('/api/auth/',authRoutes())
app.use('/api/tasks/',routes())

const server = makeStoppable(http.createServer(app));

module.exports = () => {
  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    })
  };

  return new Promise((resolve) => {
    server.listen(3000, () => {
      console.log('Express server is listening on http://localhost:3000');
      resolve(stopServer);
    });
  });
}
