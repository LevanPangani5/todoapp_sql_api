const db = require('../lib/db');


exports.addTask= async(req,res)=>{
    try{
    const {title,description}= req.body;
      if(!title || !description){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }
      console.log(req.user, 'user');
      db.query('INSERT INTO tasks (user_id, task_name, description) VALUES (?, ?, ?)', [req.user.user_id,title, description], (error) => {
        if (error) {
          console.error('Error inserting task into database:', error);
          return res.status(500).json({ message: 'An error occurred while adding the task.' });
        }
    
        res.status(201).json({ message: 'Task added successfully.' });
      });
    }catch(err){
        console.error(err);
        return res.status(500).json({error:'server error'});
      }
}

exports.getTasks= async(req,res)=>{
    try{
    db.query('SELECT * FROM tasks WHERE user_id = ?', [req.user.user_id], (error, results) => {
        if (error) {
          console.error('Error querying the database:', error);
          return res.status(500).json({ message: 'An error occurred while fetching tasks.' });
        }
    
        res.status(200).json({ tasks: results });
      });
    }catch(err){
        console.error(err);
        return res.status(500).json({error:'server error'});
      }
}

exports.makeDone =async(req,res)=>{
    try{
    const{title}= req.body;
    if(!title){
      return res.status(400).json({
        error:"Bad Request",
        message:"Schema validation of the body failed"
      })
    }

    db.query('UPDATE tasks SET isDone = 1 WHERE user_id = ? AND title = ?', [req.user.user_id, title], (error) => {
        if (error) {
          console.error('Error updating the database:', error);
          return res.status(500).json({ message: 'An error occurred while updating task status.' });
        }
    
        res.status(200).json({ message: 'Task status updated successfully.' });
      });
    }catch(err){
        console.error(err);
        return res.status(500).json({error:'server error'});
      }
}

exports.getDones = async(req,res)=>{
    try{
    db.query('SELECT * FROM tasks WHERE user_id = ? AND isDone = 1', [req.user.user_id], (error, results) => {
        if (error) {
            console.error('Error querying the database:', error);
            return res.status(500).json({ message: 'An error occurred while fetching tasks.' });
          }
      
          res.status(200).json({ tasks: results });
    })
}catch(err){
    console.error(err);
    return res.status(500).json({error:'server error'});
  }
}

exports.editTask= async(req,res)=>{
    try{
    const newTitle = req.params.taskTitle;
      const {title: currentTitle,description}= req.body;
      
      if(!currentTitle || !description||!newTitle){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }
    db.query('SELECT * FROM tasks WHERE user_id = ? AND title = ?', [req.user.user_id, currentTitle], (error, results) => {
        if (error) {
          console.error('Error querying the database:', error);
          return res.status(500).json({ message: 'An error occurred while updating task title.' });
        }
    
        if (results.length === 0) {
          return res.status(404).json({ message: 'Current title not found.' });
        }
    
        db.query('SELECT * FROM tasks WHERE user_id = ? AND title = ?', [req.user.user_id, newTitle], (error, results) => {
          if (error) {
            console.error('Error querying the database:', error);
            return res.status(500).json({ message: 'An error occurred while updating task title.' });
          }
    
          if (results.length > 0) {
            return res.status(400).json({ message: 'New title already exists.' });
          }
    
          // Update the task title
          db.query('UPDATE tasks SET title = ? WHERE user_id = ? AND title = ?', [newTitle, req.user.user_id, currentTitle], (error) => {
            if (error) {
              console.error('Error updating the database:', error);
              return res.status(500).json({ message: 'An error occurred while updating task title.' });
            }
            res.status(200).json({ message: 'Task title updated successfully.' });
          });
        });
      });
    }catch(err){
        console.error(err);
        return res.status(500).json({error:'server error'});
      }
}

exports.deleteTask =async(req,res)=>{
    try{
    const{title}= req.body;
      if(!title){
        return res.status(400).json({
          error:"Bad Request",
          message:"Schema validation of the body failed"
        })
      }

      db.query('DELETE FROM tasks WHERE user_id = ? AND title = ?', [req.user.user_id, title], (error, result) => {
        if (error) {
          console.error('Error deleting task from the database:', error);
          return res.status(500).json({ message: 'An error occurred while deleting the task.' });
        }
    
        if (result.affectedRows === 0) {
          return res.status(404).json({ message: 'Task not found.' });
        }
    
        res.status(200).json({ message: 'Task deleted successfully.' });
      });
    }catch(err){
     console.error(err);
     return res.status(500).json({error:'server error'});
      }
}