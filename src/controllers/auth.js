const db = require('../lib/db');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const dotenv = require('dotenv');

dotenv.config({path:'../.env'})

exports.register=async (req,res)=>{
   try{
    const { email, password } = req.body; 
        db.query('SELECT * FROM users WHERE email = ?', [email], (error, results) => {
            if (error) {
              console.error('Error querying the database:', error);
              return res.status(500).json({
                error:'Server error',
                 message: 'An error occurred while registering the user.' });
            }
        
            if (results.length > 0) {
              return res.status(400).json({ 
                message: 'Email already registered.' });
            }
        })
         
        const hashed =await bcrypt.hash(password,10);
        db.query('INSERT INTO users (email, password) VALUES (?, ?)', [email, hashed], (error) => {
            if (error) {
              console.error('Error inserting user into database:', error);
              return res.status(500).json({ message: 'An error occurred while registering the user.' });
            }
      
            res.status(201).json({ message: 'User registered successfully.' });
        })
        
    }catch(err){
        res.status(500).json({
            error:"Server error",  
        })
    }
    }

    exports.login = async (req,res)=>{
      try{
        const { email, password } = req.body;
       
        console.log(email);
       db.query('SELECT * FROM users WHERE email = ?',[email], async (error, results) => {
            if (error) {
              console.error('Error querying the database:', error);
              return res.status(500).json({ message: 'An error occurred while logging in.' });
            }
               console.log(results);
            if (results.length === 0) {
              return res.status(401).json({ message: 'User not found.' });
            }
        
             user = results[0];
            const valid = await bcrypt.compare(password, user.password);
        
            if (!valid) {
              return res.status(401).json({ message: 'Invalid password.' });
            }
            const token = jwt.sign({ user_id:user.id, email}, process.env.JWTSECRET, { expiresIn: '1h' });
            return res.status(200).json({
                user: { 
                  user_id:user.id,
                  email 
              }, 
              token
            })
          });
        }catch(err){
          console.error(err);
          return res.status(500).json({error:'server error'});
        }

    }
        

