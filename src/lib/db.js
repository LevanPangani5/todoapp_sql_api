const mysql = require('mysql');
const dotenv = require('dotenv');

dotenv.config({path:'../.env'})

const db=mysql.createConnection({
    host: process.env.HOST,
    user:process.env.USER,
    password:process.env.PWD,
    database:process.env.DB
  })
  
  db.connect((err)=>{
    if(err){
      console.log("failed to connect")
    }
    else{
      console.log("connected");
    }
  })

  module.exports=db;