const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const taskController = require('../controllers/tasks');

async function isAuthenticated(req, res, next) {
  const authHeader = req.headers['authorization'];
  if (authHeader && authHeader.startsWith('Bearer ')) {
    const token = authHeader.split(' ')[1];  
    try {
      const decoded = jwt.verify(token, process.env.JWTSECRET);
      req.user = decoded;
      next();
    } catch (err) {
      return res.status(401).json({
        error: 'Unauthorized',
        message: 'Invalid or expired token.',
      });
    }
  } else {
    return res.status(401).json({
      error: 'Unauthorized',
      message: 'Authorization token missing or invalid.',
    });
  }
}
router.use(isAuthenticated);

    router.post('/',taskController.addTask);

    router.get('/', taskController.getTasks);

    router.put('/done', taskController.makeDone);

    router.get('/done', taskController.getDones);

    router.put('/done/:taskTitle', taskController.editTask);

    router.delete('/done', taskController.deleteTask)

module.exports=()=>{return router}